# The datasets used for the repeated cross-validation experiments can be accessed at:
[Experiments Repository](https://drive.google.com/drive/folders/1uJbISGk-NFDDaL9uxACuLgfMohyi4-ai?usp=sharing)

# 30 files (trainining and testing) are used for the experiments described in our paper:

  **current_train** - dataset before concept drift.

  **future_train** - dataset that will receive the feedback.

  **future_train_cluster** - dataset with feedback information (the k-means cluster).
	
  **future_train_standardized** - only used for batch learning. Dataset pre-processed considering the current_train statistical values.

  **test_cluster** - dataset used for testing with feedback information (the k-means cluster).

  **test_standardized** - only used for batch learning. Dataset pre-processed considering previous statistical values.
	
	


