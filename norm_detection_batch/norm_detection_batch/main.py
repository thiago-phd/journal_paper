from norm_detection_batch.app import DetectionFlow, DetectionFlowSingle


def main():
    print("Norm Detection Training Procedures Started!!!")

    # Ensemble
    # DetectionFlow.start_process()
    # DetectionFlow.start_process_interpretability()

    # Extended Experiments - SINGLE Model
    DetectionFlowSingle.start_process()


if __name__ == '__main__':
    main()





