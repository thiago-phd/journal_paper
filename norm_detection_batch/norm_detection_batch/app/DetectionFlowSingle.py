import os
import pathlib
import pickle
import timeit

import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.metrics import confusion_matrix, classification_report
from norm_detection_batch.util import UtilDataSetFiles

from tensorflow import keras
from tensorflow.keras import layers

from norm_detection_batch.util.UtilDataSetFiles import ResampleStrategy


def start_process():
    # No Concept Drift - Since we are handling batch learning, we split the parts of the experiments (concept vs no concept).
    # train_single("/experiments/keras/datasets/", "/experiments/keras/single_trained_models/", "keras", 30, 2, evaluate_by_step=False)

    # No Concept Drift with SMOTE
    # train_single("/experiments/keras/datasets/", "/experiments/keras/single_smote_trained_models/", "keras", 30, 2, evaluate_by_step=False,
    #              resample_strategy=ResampleStrategy.SMOTE)

    # With Concept Drift
    train_single_concept("/experiments/keras/datasets/", "/experiments/keras/single_concept_trained_model/", "keras", 30, 2, evaluate_by_step=False)

    # With Concept Drift and SMOTE
    '''train_single_concept("/experiments/keras/datasets/", "/experiments/keras/single_concept_smote_trained_model/", "keras", 30, 2, evaluate_by_step=False,
                         resample_strategy=ResampleStrategy.SMOTE)'''


def train_single(dataset_directory, directory_to_save, ml_model, number_of_executions, cluster_class, evaluate_by_step=False,
                 current_dataset_weight=0.1, future_dataset_weight=10, new_label_value=0, old_label_value=1,
                 columns_to_drop=["LABEL"], step_size=512, resample_strategy=ResampleStrategy.Replication):
    print("Training (SINGLE MODEL - NO CONCEPT DRIFT) process started!!!")
    base_directory = str(pathlib.Path(__file__).parent.parent.parent)

    start_execution = 0
    for current_execution in range(start_execution, number_of_executions):
        print("Execution: " + str(current_execution + 1) + " started!!!")

        # To compare with ENSEMBLE BATCH for the NO CONCEPT DRIFT case, we use only the current_train.
        current_dataset = get_working_dataset("current_train", dataset_directory, current_execution)
        test_dataset = get_working_dataset("test", dataset_directory, current_execution)
        training_standardized, test_standardized = pre_process(current_dataset, test_dataset)

        training_standardized["WEIGHT"] = 1  # The balanced method expects the column WEIGHT as standard format. Not used in the training process.
        balanced_datasets, _ = UtilDataSetFiles.handle_imbalanced_dataset(training_standardized, "", "", initial_number_of_classifiers=1,
                                                                          resample_strategy=resample_strategy)
        single_balanced_dataset = balanced_datasets[0]  # There will be only one balanced dataset in this case.

        x_train = single_balanced_dataset.drop(columns_to_drop, axis=1)
        y_train = single_balanced_dataset['LABEL']

        train_execution_time = 0
        dataset_contain_weight = False
        # Since we are only handling BEFORE concept drift occurs, then there will be NO weights.
        if "WEIGHT" in single_balanced_dataset.columns.values:
            x_train = x_train.drop(["WEIGHT"], axis=1)

        if ml_model == "keras":
            model, elapsed_time = train_with_keras(x_train, y_train, dataset_contain_weight)
            train_execution_time += elapsed_time
        else:
            raise Exception("ML Model not recognized!")

        information_to_save = {
            "current_execution": current_execution,
            "test_results": {},
            "test_re_label_results": {},
            "train_execution_time": train_execution_time
        }

        if ml_model == "keras":
            information_to_save["test_results"] = evaluate_single_model_keras(model, test_standardized,
                                                                              columns_to_drop)

            current_execution_directory = directory_to_save + str(current_execution)
            save_single_keras_model(model, "model", current_execution_directory)

            information_file_path = base_directory + current_execution_directory + "/overall_test_results" + ".pickle"
            pickle.dump(information_to_save, open(information_file_path, 'wb'))

        print("Execution: " + str(current_execution + 1) + " finished!!!")


def train_single_concept(dataset_directory, directory_to_save, ml_model, number_of_executions, cluster_class, evaluate_by_step=False,
                         current_dataset_weight=0.1, future_dataset_weight=10, new_label_value=0, old_label_value=1,
                         columns_to_drop=["LABEL", "CLUSTER"], step_size=512, resample_strategy=ResampleStrategy.Replication):
    print("Training (SINGLE MODEL - CONCEPT DRIFT) process started!!!")
    base_directory = str(pathlib.Path(__file__).parent.parent.parent)

    start_execution = 17
    for current_execution in range(start_execution, number_of_executions):
        print("Execution: " + str(current_execution + 1) + " started!!!")

        current_dataset = get_working_dataset("current_train", dataset_directory, current_execution)
        current_dataset["CLUSTER"] = -1  # Adding this to have similar columns in the datasets.
        current_dataset["WEIGHT"] = current_dataset_weight

        future_dataset = get_working_dataset("future_train_cluster", dataset_directory, current_execution)
        future_dataset["WEIGHT"] = future_dataset_weight

        complete_dataset = pd.concat([current_dataset, future_dataset]).sample(frac=1)

        test_dataset = get_working_dataset("test_cluster", dataset_directory, current_execution)
        training_standardized, test_standardized = pre_process(complete_dataset, test_dataset)

        training_re_labeled, only_re_label = apply_re_label(training_standardized,
                                                            cluster_class, new_label_value,
                                                            old_label_value, resample_strategy=resample_strategy)

        # Besides re-labeling the data, we also oversampled the data points that were re-labeled.
        training_with_over_re_label = pd.concat([training_re_labeled, only_re_label]).sample(frac=1)

        balanced_datasets, _ = UtilDataSetFiles.handle_imbalanced_dataset(training_with_over_re_label, "", "", initial_number_of_classifiers=1,
                                                                          resample_strategy=resample_strategy)
        single_balanced_dataset = balanced_datasets[0]  # There will be only one balanced dataset in this case.

        x_train = single_balanced_dataset.drop(columns_to_drop, axis=1)
        y_train = single_balanced_dataset['LABEL']

        train_execution_time = 0
        training_weights = []
        dataset_contain_weight = True
        if "WEIGHT" in single_balanced_dataset.columns.values:
            x_train = x_train.drop(["WEIGHT"], axis=1)
            training_weights = single_balanced_dataset['WEIGHT']

        if ml_model == "keras":
            model, elapsed_time = train_with_keras(x_train, y_train, dataset_contain_weight, training_weights)
            train_execution_time += elapsed_time
        else:
            raise Exception("ML Model not recognized!")

        information_to_save = {
            "current_execution": current_execution,
            "test_results": {},
            "test_re_label_results": {},
            "train_execution_time": train_execution_time
        }

        if ml_model == "keras":
            test_re_labeled, test_only_re_label = apply_re_label(test_standardized,
                                                                 cluster_class, new_label_value,
                                                                 old_label_value, resample_strategy=resample_strategy)
            test_re_labeled = test_re_labeled.drop(["CLUSTER"], axis=1)
            test_only_re_label = test_only_re_label.drop(["CLUSTER"], axis=1)

            information_to_save["test_results"] = evaluate_single_model_keras(model, test_re_labeled)
            information_to_save["test_re_label_results"] = evaluate_re_label(model, test_only_re_label)

            current_execution_directory = directory_to_save + str(current_execution)
            save_single_keras_model(model, "model", current_execution_directory)

            information_file_path = base_directory + current_execution_directory + "/overall_test_results" + ".pickle"
            pickle.dump(information_to_save, open(information_file_path, 'wb'))

        print("Execution: " + str(current_execution + 1) + " finished!!!")


def get_working_dataset(file_name, directory, index):
    complete_file_name = str(index) + "_" + file_name + ".csv"
    dataset = UtilDataSetFiles.get_data_set_from_file(directory, complete_file_name)

    return dataset


def pre_process(training_dataset, testing_dataset, columns_not_altered=[57], columns_not_altered_test=[57]):
    number_of_columns_training_dataset = len(training_dataset.columns)
    if number_of_columns_training_dataset > 58:
        for i in range(57, number_of_columns_training_dataset):
            columns_not_altered.append(i)

    number_of_columns_testing_dataset = len(testing_dataset.columns)
    if number_of_columns_testing_dataset > 58:
        for i in range(57, number_of_columns_testing_dataset):
            columns_not_altered_test.append(i)

    training_dataset, testing_dataset = get_dataset_no_null_values(training_dataset, testing_dataset,
                                                                   columns_not_altered)

    scaled_training, scaled_values = UtilDataSetFiles.apply_standardization(training_dataset.copy(),
                                                                            columns_not_altered)
    scaled_testing = UtilDataSetFiles.apply_standardization_defined(testing_dataset.copy(), scaled_values,
                                                                    columns_not_altered_test)

    return scaled_training, scaled_testing


def get_dataset_no_null_values(training_dataset, testing_dataset, columns_not_altered=[57]):
    training_dataset, columns_statistical_values = UtilDataSetFiles.handle_missing_values(training_dataset.copy(),
                                                                                          columns_not_altered)
    testing_dataset = UtilDataSetFiles.update_missing_values_media_mode(testing_dataset.copy(),
                                                                        columns_statistical_values)

    return training_dataset, testing_dataset


def train_with_keras(x_train, y_train, train_with_weight=False, weights=[]):
    start_time = timeit.default_timer()

    model = keras.Sequential([
        layers.Dense(6, activation="relu", input_shape=(57,), name="first_hidden_layer"),
        layers.Dense(3, activation="relu", name="second_hidden_layer"),
        layers.Dense(2, activation="softmax", name="output_layer")
    ])

    model.compile(
        optimizer=keras.optimizers.SGD(learning_rate=0.01),
        loss=keras.losses.SparseCategoricalCrossentropy(),
        metrics=[keras.metrics.SparseCategoricalAccuracy()]
    )

    if train_with_weight:
        model.fit(x_train, y_train, epochs=512, batch_size=len(x_train), sample_weight=weights, verbose=0)
    else:
        model.fit(x_train, y_train, epochs=512, batch_size=len(x_train), verbose=0)

    finish_time = timeit.default_timer()
    elapsed_time = finish_time - start_time

    return model, elapsed_time


def evaluate_single_model_keras(trained_model, testing_dataset, labels_to_drop=["LABEL"]):
    x_test = testing_dataset.drop(labels_to_drop, axis=1)
    y_test = pd.Series(data=testing_dataset['LABEL'], dtype='int32')

    y_real = y_test.values.tolist()
    y_probabilities = trained_model.predict(x_test)
    y_predicted = np.argmax(y_probabilities, axis=1)

    calculated_classification_report = classification_report(y_real, y_predicted)
    print(calculated_classification_report)

    calculated_confusion_matrix = confusion_matrix(y_test, y_predicted)
    print(calculated_confusion_matrix)

    corrected_classified_regular = calculated_confusion_matrix[0][0] / (calculated_confusion_matrix[0][0] +
                                                                        calculated_confusion_matrix[0][1])

    corrected_classified_vandalism = calculated_confusion_matrix[1][1] / (calculated_confusion_matrix[1][0] +
                                                                          calculated_confusion_matrix[1][1])

    overall_recall = (corrected_classified_regular + corrected_classified_vandalism) / 2

    print("OVERALL RECALL: " + str(overall_recall))

    test_results = {
        "overall_recall": overall_recall,
        "confusion_matrix": calculated_confusion_matrix,
        "y_real": y_real,
        "y_probabilities": y_predicted
    }

    return test_results


def evaluate_re_label(trained_model, testing_dataset, re_label_to=0, labels_to_drop=["LABEL"]):
    x_test = testing_dataset.drop(labels_to_drop, axis=1)
    y_test = pd.Series(data=testing_dataset['LABEL'], dtype='int32')

    y_real = y_test.values.tolist()
    y_probabilities = trained_model.predict(x_test)
    y_predicted = np.argmax(y_probabilities, axis=1)

    calculated_classification_report = classification_report(y_real, y_predicted)
    print(calculated_classification_report)

    calculated_confusion_matrix = confusion_matrix(y_test, y_predicted)
    print(calculated_confusion_matrix)

    test_results = {
        "confusion_matrix": calculated_confusion_matrix,
        "y_real": y_real,
        "y_probabilities": y_probabilities
    }

    return test_results


def apply_re_label(dataset, cluster_class_to_re_label, new_label_value, old_label_value, replicate_re_label=True,
                   resample_strategy=ResampleStrategy.Replication):
    dataset.loc[dataset["CLUSTER"] == cluster_class_to_re_label, "LABEL"] = new_label_value
    re_label_data = dataset[dataset["CLUSTER"] == cluster_class_to_re_label]
    number_of_re_labelled_data = len(re_label_data)

    if replicate_re_label and number_of_re_labelled_data > 0:
        number_of_instances_after_re_label = len(dataset[dataset["LABEL"] == old_label_value])
        amount_to_replicate = np.math.ceil(number_of_instances_after_re_label / number_of_re_labelled_data)

        if resample_strategy == ResampleStrategy.Replication:
            re_label_data_replicated = pd.concat([re_label_data] * amount_to_replicate, ignore_index=True)
        elif resample_strategy == ResampleStrategy.SMOTE:
            desired_number_of_re_label = number_of_re_labelled_data * amount_to_replicate
            if desired_number_of_re_label > number_of_re_labelled_data:
                re_label_data_replicated = UtilDataSetFiles.apply_smote_re_label(dataset, re_label_data, desired_number_of_re_label,
                                                                                 cluster_class_to_re_label, new_label_value)
            else:
                re_label_data_replicated = re_label_data

        return dataset, re_label_data_replicated
    else:
        return dataset, re_label_data


def save_single_keras_model(trained_model, folder_name, path_to_save):
    base_directory = str(pathlib.Path(__file__).parent.parent.parent)

    complete_path_to_save = base_directory + path_to_save
    create_folder_to_save(complete_path_to_save)

    file_path = complete_path_to_save + "/" + folder_name + "0"
    trained_model.save(file_path)


def create_folder_to_save(complete_path_to_save):
    if not os.path.exists(complete_path_to_save):
        os.mkdir(complete_path_to_save)


